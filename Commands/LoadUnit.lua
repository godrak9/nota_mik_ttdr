function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "load given unit",
		parameterDefs = {
			{
				name = "transportID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringGetUnitTransporter = Spring.GetUnitTransporter
local SpringGetUnitIsDead = Spring.GetUnitIsDead

function Run(self, units, parameter)
	local transportID = parameter.transportID
	local unitID = parameter.unitID
	local cmdID = CMD.LOAD_UNITS

	if (SpringGetUnitIsDead(transportID) ~= false) then
		return FAILURE
	end

	if (SpringGetUnitIsDead(unitID) ~= false) then
		return FAILURE
	end

	if (SpringGetUnitIsTransporting(transportID)[1] ~= nil) then
		if (SpringGetUnitTransporter(unitID) ~= transportID) then
			return FAILURE
		else
			return SUCCESS
		end
	end

	if SpringGetUnitTransporter(unitID) ~= nil then
		return FAILURE
	end

	SpringGiveOrderToUnit(transportID, cmdID, {unitID}, {})
	return RUNNING
end


function Reset(self)
end
