function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "unload unit",
		parameterDefs = {
			{
				name = "transportID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "area",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringGetUnitTransporter = Spring.GetUnitTransporter
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitIsDead = Spring.GetUnitIsDead

local OrderGiven = false

function Run(self, units, parameter)
	local transportID = parameter.transportID
	local area = parameter.area
	local cmdID = CMD.UNLOAD_UNITS

	if (SpringGetUnitIsDead(transportID) ~= false) then
		return FAILURE
	end


	if (SpringGetUnitIsTransporting(transportID)[1] == nil) then
		return SUCCESS
	end

	if OrderGiven then
		return RUNNING
	end

	local x,y,z = SpringGetUnitPosition(transportID)

	SpringGiveOrderToUnit(transportID, cmdID, {x,y,z,area.radius}, {})
	OrderGiven = true
	return RUNNING
end


function Reset(self)
	OrderGiven = false
end
