function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD = 200

local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsDead = Spring.GetUnitIsDead

local function ClearState(self)
end

function Run(self, units, parameter)
	local position = parameter.position
	local unitID = parameter.unitID
	local cmdID = CMD.MOVE

	if (SpringGetUnitIsDead(unitID) ~= false) then
		return FAILURE
	end

	local targetPosition = Vec3(position.x, position.y, position.z)

	local pointX, pointY, pointZ = SpringGetUnitPosition(unitID)
	local unitPosition = Vec3(pointX, pointY, pointZ)

	if (unitPosition:Distance(targetPosition) < THRESHOLD) then
		return SUCCESS
	else
		SpringGiveOrderToUnit(unitID, cmdID, targetPosition:AsSpringVector(), {})
		return RUNNING
	end
end


function Reset(self)
end
