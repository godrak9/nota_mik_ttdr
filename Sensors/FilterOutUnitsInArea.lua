local sensorInfo = {
	name = "",
	desc = "Return safe location",
	author = "PavelMikus",
	date = "",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT,
		tooltip = "Filter out units in given area"
	}
end

local SpringGetUnitPosition = Spring.GetUnitPosition

return function(listOfUnits, area)
	local filtered = {}
	local center = Vec3(area.center.x, area.center.y, area.center.z)
	local radius = area.radius
	for i=1, #listOfUnits do
		local x,y,z = SpringGetUnitPosition(listOfUnits[i])
		if center:Distance(Vec3(x,y,z)) > radius then
			filtered[#filtered+1] = listOfUnits[i]
		end
	end
    return filtered
end
